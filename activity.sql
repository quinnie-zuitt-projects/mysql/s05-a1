
-- customer name from the Philippines
SELECT customerName FROM customer 
WHERE City = 'Philippines';

-- contactLastname and contactFirstName with "La Rochelle Gifts"
SELECT contactLastname, contactFirstName 
FROM customer
WHERE customerName = 'La Rochelle Gifts';

-- no. 3
SELECT productName, MSRP 
FROM products
WHERE productName = 'The Titanic';

-- no. 4
SELECT lastName, firstName
FROM employees
WHERE email = 'jfirrelli@classicmodelcars.com';

-- no. 5
SELECT customerName
FROM customer
WHERE state IS NULL;

-- no. 6
SELECT lastName, firstName, email
FROM employees
WHERE lastName = 'Patterson' AND firstName = 'Steve';

-- no. 7
SELECT customerName, country, creditLimit
FROM customer
WHERE NOT country = 'USA' AND creditLimit > 3000;

-- no. 8 
SELECT customerName
FROM customer
WHERE customerName NOT LIKE '%a%';

-- no.9
SELECT COUNT(orderNumber) 
FROM orders
WHERE comments LIKE '%DHL%';

-- no. 10
SELECT productLine
FROM productlines
WHERE textDescription LIKE '%state of the art%';

-- no. 11
SELECT DISTINCT country FROM customer;

-- no. 12
SELECT DISTINCT status FROM orders;

-- no. 13
SELECT customerName, country FROM customer;
WHERE country IN  ('USA', 'France', 'Canada');

-- no. 14
SELECT employees.firstName, employees.lastName, offices.city 
FROM employees 
INNER JOIN offices 
ON employees.officeCode = offices.officeCode 
WHERE offices.city = "Tokyo";

-- no. 15
SELECT customerName FROM customer
WHERE salesRepEmployeeNumber 
IN(SELECT employeeNumber FROM employees WHERE lastName = 'Thompson' AND firstName ='Leslie');

-- no. 16 
SELECT products.productName, customers.customerName FROM products 
INNER JOIN customers 
WHERE customers.customerName = "Baane Mini Imports";

-- no. 17
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country 
FROM employees 
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber 
JOIN offices ON employees.officeCode = offices.officeCode 
WHERE customers.country = offices.country;

-- no. 18
SELECT lastName, firstName 
FROM employees 
WHERE reportsTo 
IN(SELECT employeeNumber FROM employees WHERE firstName="Anthony" AND lastName="Bow");

-- no. 19
SELECT productName, MAX(MSRP)  
FROM products; 

-- no. 20
SELECT COUNT(*) 
FROM customers 
WHERE country="UK";

-- no. 21 
SELECT COUNT(*) 
FROM products 
WHERE productLine 
IN (SELECT productLine FROM productlines) GROUP BY productLine;

-- no. 22
SELECT COUNT(*) 
FROM customers 
WHERE salesRepEmployeeNumber 
IN(SELECT employeeNumber FROM employees) GROUP BY salesRepEmployeeNumber;

-- no. 23
SELECT productName, quantityInStock 
FROM products 
WHERE productLine="Planes" AND quantityInStock < 1000;
